function OES = compute_overall_score(scores_ODC, scores_OFQ)
    
    mean_IoU = 1 / 2 * (scores_ODC.mIoU_proximity + scores_OFQ.mIoU_ideal);

    OES = scores_ODC.ODCS * 1 / 4 * (scores_ODC.JQCS + scores_ODC.Dyn_F1 + scores_OFQ.JFMS + mean_IoU);
end