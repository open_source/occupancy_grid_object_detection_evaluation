function [footprint_quality_reference, gt_shape, f_gt_shape] = compute_polyshapes_and_footprint_quality_reference(pointcloud_objects, f_pointcloud, GT_objects, f_GT_objects, parameters)

    % Input
    % - LiDAR point cloud with a point per row. It should only contain points relative to the ground truth objects
    % - Ground truth objects with an object per row. Objects (i) outside the grid range, (ii) with a reduced number of points and (iii) pedestrians inside vehicles should be excluded  

    % Output
    % - footprint_quality_reference: The value obtained for the quality footprint reference
    % - gt_shape: The computed polyshapes
    

    
        
    %% Compute polyshapes and footprint quality score
    footprint_quality_reference = zeros(size(GT_objects, 1), 1);
    gt_shape = cell(size(GT_objects, 1), 5);

    f_gt_shape.polyshape_pointcloud = 1;
    f_gt_shape.polyshape_box = 2;

    minimum_number_of_points_for_convhull = 3;
    for i_gt = 1 : size(GT_objects, 1)

        % The quality with which a sensed footprint models the real shape and location of an object is computed as the 
        % Intersection over Union (IoU) between the bounding box of the ground truth and the convex hull of the impacts on the object.

        % Ground truth bounding box
        corners = compute_corners_of_box(GT_objects(i_gt, f_GT_objects.px), GT_objects(i_gt, f_GT_objects.py), GT_objects(i_gt, f_GT_objects.yaw), GT_objects(i_gt, f_GT_objects.length), GT_objects(i_gt, f_GT_objects.width));

        % Find the points of the point cloud gathered inside the bounding box
        [in, on] = inpolygon(pointcloud_objects(:, f_pointcloud.x), pointcloud_objects(:, f_pointcloud.y), corners(:, 1), corners(:, 2));
        points_gathered = in == 1 | on == 1;
        number_of_points_inside = sum(points_gathered);
        obj_pointcloud = pointcloud_objects(points_gathered, :);        

        % Bounding box's polyshape
        polyshape_box = polyshape(corners(:, 1), corners(:, 2));

        % If possible, compute convexhull
        collinear = check_collinear(corners);

        if(number_of_points_inside > minimum_number_of_points_for_convhull && collinear == false)
            idx_convhull = convhull(obj_pointcloud(:, f_pointcloud.x), obj_pointcloud(:, f_pointcloud.y));
            polyshape_pointcloud = polyshape(obj_pointcloud(idx_convhull, f_pointcloud.x), obj_pointcloud(idx_convhull, f_pointcloud.y));

            IoU_pc = compute_IoU_polygons(polyshape_pointcloud, polyshape_box);
        else
            polyshape_pointcloud = [];
            IoU_pc = 0;
        end

        % Save results
        footprint_quality_reference(i_gt, 1) = IoU_pc;

        gt_shape{i_gt, f_gt_shape.polyshape_pointcloud} = polyshape_pointcloud;
        gt_shape{i_gt, f_gt_shape.polyshape_box} = polyshape_box;
    end


    %% Debug plot
    if(parameters.plot_footprint_quality_reference)
        
        figure, hold on, axis equal, 
        plot(pointcloud_objects(:, f_pointcloud.x), pointcloud_objects(:, f_pointcloud.y), '.r', 'MarkerSize', 10);

        for i_gt = 1 : size(GT_objects, 1)
            % Plot point cloud polyshape
            if(~isempty(gt_shape{i_gt, f_gt_shape.polyshape_pointcloud}))
                plot(gt_shape{i_gt, f_gt_shape.polyshape_pointcloud}, 'FaceAlpha', 0.25, 'EdgeAlpha', 0, 'FaceColor', [0, 0, 1])
            end
                
            % Plot grount truth object bounding box
            plot(gt_shape{i_gt, f_gt_shape.polyshape_box}, 'FaceAlpha', 0.25, 'EdgeAlpha', 0, 'FaceColor', [0, 0.8, 0])
    
            % Score
            pos = mean(gt_shape{i_gt, f_gt_shape.polyshape_box}.Vertices);
            text(pos(1), pos(2), ['FQR ', num2str(round(footprint_quality_reference(i_gt), 2))])            
        end
    end

end