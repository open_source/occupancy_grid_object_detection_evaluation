function [box, f_box, min_var] = compute_bounding_box(points, angular_step, option_method, closeness_minimum_distance_threshold)        
    
% Based on: Efficient L-Shape Fitting for Vehicle Detection Using Laser Scanners

if(nargin < 4 && option_method == 3)
    closeness_minimum_distance_threshold = 0.1;
end

%{
Example:

% Defining points
points_x = [linspace(1, 12, 100) + normrnd(0, 1, [1, 100]); 2.5 * rand(1, 100)]'; 
points_y = [2 * rand(1, 100); linspace(1, 5, 100) + normrnd(0, 0.5, [1, 100])]';
points = [points_x; points_y];
[points(:, 1), points(:, 2)] = rotation_matrix(points(:, 1), points(:, 2), 20 * pi / 180);

% Define the angular step 
angular_step = 0.1 * pi / 180;



figure, hold on, axis equal, 

% Variance
box_var = compute_bounding_box(points, angular_step, 1);
corners = compute_corners_of_box(box_var(1), box_var(2), box_var(3), box_var(4), box_var(5));
plot([corners(:, 1); corners(1, 1)], [corners(:, 2); corners(1, 2)], '-r')
plot(points(:, 1), points(:, 2), '.k')

legend('Variance', 'Area', 'Closeness', 'Points')
%}

    min_var = inf;

    for ang = 0 : angular_step : (pi - angular_step)
        
        e_1 = [cos(ang), sin(ang)];
        e_2 = [-sin(ang), cos(ang)];

        C_1 = zeros(size(points, 1), 1);    
        C_2 = zeros(size(points, 1), 1);

        for i = 1 : size(points, 1)
            C_1(i, 1) = points(i, :) * e_1';
            C_2(i, 1) = points(i, :) * e_2';
        end

        score = based_on_variance(C_1, C_2);

        if(score < min_var)
            min_var = score;
            
            box_angle = ang;
    
            box_length = max(C_1) - min(C_1);
            box_width = max(C_2) - min(C_2);
    
            box_center_x = min(C_1) + box_length / 2;
            box_center_y = min(C_2) + box_width / 2;
    
            [box_center_x, box_center_y] = rotation_matrix(box_center_x, box_center_y, box_angle);
        end
    end
    
    
    box = [box_center_x, box_center_y, box_angle, box_length, box_width];   
    f_box.px = 1;
    f_box.py = 2;
    f_box.yaw = 3;
    f_box.length = 4;
    f_box.width = 5;
end

%% Function variance
function variance = based_on_variance(C_1, C_2)
    
    
    C_1_max = max(C_1);
    C_1_min = min(C_1);
    
    C_2_max = max(C_2);
    C_2_min = min(C_2);
    
    D_1 = min((C_1_max - C_1), (C_1 - C_1_min));
    D_2 = min((C_2_max - C_2), (C_2 - C_2_min));
    
    E_1 = D_1(D_1 < D_2);
    E_2 = D_2(D_2 < D_1);
    
    if(isempty(E_1))
        var_1 = 0;    
        var_2 = var(E_2);
        
    elseif(isempty(E_2))
        var_1 = var(E_1);
        var_2 = 0;
        
    else    
        var_1 = var(E_1);
        var_2 = var(E_2);
    end
    if(isnan(var_1))
        var_1 = 0;
    end
    if(isnan(var_2))
        var_2 = 0;
    end

    variance = var_1 + var_2;
end
    




