function final_score_ODC = final_scores_object_detection_capability(data_ODC, idx_desired_objects, parameters)

    % Input
    %  - data_ODC: table containing the data for object detectetion capability, compute with single_iteration_object_detection_capability (concatenate data for multiple iterations)  
    %  - idx_desired_objects: array (size(data_ODC, 1) x 1) denoting which objects are selected [0, 1]. e.g. you want to compute results just for vehicles        
    %  - parameters: struct contaning the parametrization
    %
    % Output
    %  - final_score_ODC = table contanining the scores obtained for object detection capability 

    %% Select objects
    desired_ODC = data_ODC(idx_desired_objects == 1, :);

    % detected objects for mIoU and Dyn segmentation
    idx_detected = desired_ODC.detected == 1;


    if(sum(idx_detected) == 0)
        fprintf("final_scores_object_detection_capability - No objects detected\n")

        if(parameters.dynamic_occupancy_grid)
            final_score_ODC = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            final_score_ODC = array2table(final_score_ODC);
            final_score_ODC.Properties.VariableNames = {'ODCS', 'JQCS', 'QCS_noise', 'QCS_merge', 'QCS_split', 'n_detected', 'n_noise', 'n_merge', 'n_split', 'mIoU_proximity', 'Dyn_Precision', 'Dyn_Recall', 'Dyn_F1'};  
        else
            final_score_ODC = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            final_score_ODC = array2table(final_score_ODC);
            final_score_ODC.Properties.VariableNames = {'ODCS', 'JQCS', 'QCS_noise', 'QCS_merge', 'QCS_split', 'n_detected', 'n_noise', 'n_merge', 'n_split', 'mIoU_proximity'};   
        end

        return
    end

    
    %% Final scores
    n_objects = size(desired_ODC, 1);

    % Object detection capability score  
    n_detected = sum(desired_ODC.detected == 1);  
    ODCS = sum(desired_ODC.detected == 1) / n_objects;

    % Quality clustering scores
    n_merge = sum(desired_ODC.merge(idx_detected) == 1);
    n_split = sum(desired_ODC.split(idx_detected) == 1);
    n_noise = sum(desired_ODC.noise(idx_detected) == 1);    

    n_detected_perfect = sum(desired_ODC.merge(idx_detected) == 0 & desired_ODC.split(idx_detected) == 0 & desired_ODC.noise(idx_detected) == 0);
    n_problems = sum(desired_ODC.merge(idx_detected) == 1 | desired_ODC.split(idx_detected) == 1 | desired_ODC.noise(idx_detected) == 1);
    if(n_detected_perfect + n_problems ~= n_detected)
        error
    end

    QCS_merge = 1 - n_merge / n_detected;
    QCS_split = 1 - n_split / n_detected;
    QCS_noise = 1 - n_noise / n_detected;
            
    JQCS = 1 / 3 * (QCS_noise + QCS_merge + QCS_split);

    % Shape and location (IoU)
    mIoU_proximity = mean(desired_ODC.IoU(idx_detected));
    

    % Dynamic segmentation
    if(parameters.dynamic_occupancy_grid)
        TP = sum(desired_ODC.Dyn_class(idx_detected) == parameters.TP);
        FP = sum(desired_ODC.Dyn_class(idx_detected) == parameters.FP);
        TN = sum(desired_ODC.Dyn_class(idx_detected) == parameters.TN);
        FN = sum(desired_ODC.Dyn_class(idx_detected) == parameters.FN);
        
        if(TP + FP + TN + FN ~= n_detected)
            error
        end

        Dyn_Precision = TP / (TP + FP);
        Dyn_Recall = TP / (TP + FN);
    
        Dyn_F1 = 2 * Dyn_Precision * Dyn_Recall / (Dyn_Precision + Dyn_Recall);
    end

    %% Final score

    if(parameters.dynamic_occupancy_grid)
        final_score_ODC = [ODCS, JQCS, QCS_noise, QCS_merge, QCS_split, n_detected, n_noise, n_merge, n_split, mIoU_proximity, Dyn_Precision, Dyn_Recall, Dyn_F1];
        final_score_ODC = array2table(final_score_ODC);
        final_score_ODC.Properties.VariableNames = {'ODCS', 'JQCS', 'QCS_noise', 'QCS_merge', 'QCS_split', 'n_detected', 'n_noise', 'n_merge', 'n_split', 'mIoU_proximity', 'Dyn_Precision', 'Dyn_Recall', 'Dyn_F1'};  
    else
        final_score_ODC = [ODCS, JQCS, QCS_noise, QCS_merge, QCS_split, n_detected, n_noise, n_merge, n_split, mIoU_proximity];
        final_score_ODC = array2table(final_score_ODC);
        final_score_ODC.Properties.VariableNames = {'ODCS', 'JQCS', 'QCS_noise', 'QCS_merge', 'QCS_split', 'n_detected', 'n_noise', 'n_merge', 'n_split', 'mIoU_proximity'};   
    end
end