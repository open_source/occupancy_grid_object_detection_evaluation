# Occupancy Grid Evaluation Framework
This code provides an object-based evaluation framework for occupancy grids. 
It addresses the two main steps of object detection: (i) object segmentation and (ii) object level features estimation. 
For more details, please check the article "Object-wise Comparison of LiDAR Occupancy Grid Mapping Methods" [doi: 10.1016/j.robot.2023.104363](https://doi.org/10.1016/j.robot.2023.104363)

![](example.png)


## Usage example
main_example.m



## Expected data

Occupancy grid:
 - The occupancy grid is assumed to be located with respect the vehicle's reference system (VRS). 
   Thus, cells' position (with respect de VRS) is constant. 
   If not, modify the variables: min_x, min_y, cells_centers_x, cells_centers_y and cells_data 
 - The size of the cells is assumed to be constant and equal for every cell.
 - Columns refer to the distance towards the vehicle along the X edge, and Rows along the Y edge 

Ground truth data:
 - Ground truth data should be refered to VRS
 - Ground truth objects should not be contained ones inside others (e.g. delete people inside vehicles)
 - Ground truth objects outside the OG range have to be deleted
 - Point cloud data is only required for the footprint quality reference and its ground truth is recomended but not essential 

