function corners = compute_corners_of_box(center_x, center_y, theta, length, width)
    
    %% Esquinas
    % Delante derecha
    corners(1, 1) = length / 2;
    corners(1, 2) = - width / 2;
    [corners(1, 1), corners(1, 2)] = rotation_matrix(corners(1, 1), corners(1, 2), theta);
    corners(1, :) = corners(1, :) + [center_x, center_y];
    
    % Detras derecha
    corners(2, 1) = - length / 2;
    corners(2, 2) = - width / 2;
    [corners(2, 1), corners(2, 2)] = rotation_matrix(corners(2, 1), corners(2, 2), theta);
    corners(2, :) = corners(2, :) + [center_x, center_y];
    
    % Detras izquierda
    corners(3, 1) = - length / 2;
    corners(3, 2) = width / 2;
    [corners(3, 1), corners(3, 2)] = rotation_matrix(corners(3, 1), corners(3, 2), theta);
    corners(3, :) = corners(3, :) + [center_x, center_y];
    
    % Delante izquierda
    corners(4, 1) = length / 2;
    corners(4, 2) = width / 2;
    [corners(4, 1), corners(4, 2)] = rotation_matrix(corners(4, 1), corners(4, 2), theta);
    corners(4, :) = corners(4, :) + [center_x, center_y];
end