function [estimated_objects, f_object, object_cells_data, object_polyshapes, object_boxes, f_box] = compute_objects_from_clustered_map(...
    compute_polyshapes, compute_bounding_boxes, OG_clustered, OG_occupied, OG_observed_occupied, OG_vx, OG_vy, parameters)

    if(parameters.dynamic_occupancy_grid == false)
        OG_vx = zeros(parameters.ROWS, parameters.COLS);
        OG_vy = zeros(parameters.ROWS, parameters.COLS);
    end

    max_ID_objects = max(OG_clustered(:));

    f_object.ID = 1;
    f_object.px = 2;
    f_object.py = 3;
    f_object.vx = 4;
    f_object.vy = 5;
    f_object.n_cells = 6;
    f_object.n_cells_obs = 7;

    cells_centers_x = parameters.cells_centers_x;
    cells_centers_y = parameters.cells_centers_y;
    ROWS = parameters.ROWS; COLS = parameters.COLS;

    res_div2 = parameters.cell_size / 2;    

    %% Extract info from cells
    estimated_objects = zeros(max_ID_objects, numel(fields(f_object)));
    points = cell(max_ID_objects, 1);
    object_cells_data = cell(max_ID_objects, 1);
    for i_r = 1 : ROWS
        for i_c = 1 : COLS
            if(OG_clustered(i_r, i_c) > 0)
                id = OG_clustered(i_r, i_c);
                
                % Save ID
                estimated_objects(id, f_object.ID) = id;  
                
                % Save data of the cells
                aux_centers = [cells_centers_x(i_c), cells_centers_y(i_r)];
                object_cells_data{id}(end + 1, :) = [aux_centers, ...
                                                     OG_vx(i_r, i_c), OG_vy(i_r, i_c), ...
                                                     OG_occupied(i_r, i_c), OG_observed_occupied(i_r, i_c)];
                
                % If required, save cells corners
                if(compute_polyshapes || compute_bounding_boxes)                    
                    points{id}(end + 1, :) = round(aux_centers + [+res_div2, +res_div2], 5);
                    points{id}(end + 1, :) = round(aux_centers + [+res_div2, -res_div2], 5);
                    points{id}(end + 1, :) = round(aux_centers + [-res_div2, -res_div2], 5);
                    points{id}(end + 1, :) = round(aux_centers + [-res_div2, +res_div2], 5);                     
                end
            end
        end
    end
    

    % Delete empty rows     
    idx_delete = estimated_objects(:, f_object.ID) == 0;
    estimated_objects(idx_delete, :) = [];
    points(idx_delete, :) = [];
    object_cells_data(idx_delete, :) = [];

    % Solve means
    for i_obj = 1 : size(estimated_objects, 1)
        aux_sum_peso = sum(object_cells_data{i_obj}(:, 5));
        estimated_objects(i_obj, f_object.px) = sum(object_cells_data{i_obj}(:, 1) .* object_cells_data{i_obj}(:, 5)) / aux_sum_peso;
        estimated_objects(i_obj, f_object.py) = sum(object_cells_data{i_obj}(:, 2) .* object_cells_data{i_obj}(:, 5)) / aux_sum_peso;
        estimated_objects(i_obj, f_object.vx) = sum(object_cells_data{i_obj}(:, 3) .* object_cells_data{i_obj}(:, 5)) / aux_sum_peso;
        estimated_objects(i_obj, f_object.vy) = sum(object_cells_data{i_obj}(:, 4) .* object_cells_data{i_obj}(:, 5)) / aux_sum_peso; 

        % Count cells
        estimated_objects(i_obj, f_object.n_cells)     = sum(object_cells_data{i_obj}(:, 5) >= parameters.clustering_threshold_occupancy);
        estimated_objects(i_obj, f_object.n_cells_obs) = sum(object_cells_data{i_obj}(:, 6) >= parameters.clustering_threshold_occupancy);
    end
    

    %% Compute polyshapes
    num_objects = size(estimated_objects, 1);
    object_polyshapes = cell(num_objects, 2);
    if(compute_polyshapes)
        for i_obj = 1 : num_objects
    
            aux_points = points{i_obj};
            
            % Get only the points of the convex hull            
            idx_convhull = convhull(aux_points(:, 1), aux_points(:, 2));   
            points_convhull = aux_points(idx_convhull, :);
                
            % Polyshape of the object
            warning('off') % For polyshape
            object_polyshapes{i_obj, 1} = polyshape(points_convhull);
            warning('on') % For polyshape
            object_polyshapes{i_obj, 2} = estimated_objects(i_obj, f_object.ID);                
        end
    end


    %% Compute bounding boxes
    object_boxes = cell(num_objects, 2);
    if(compute_bounding_boxes)
        for i_obj = 1 : num_objects
    
            aux_points = points{i_obj};
           
            [box, f_box] = compute_bounding_box(aux_points, parameters.bounding_box_fitting_angular_step, 1); 

            
            % Polyshape of the object
            object_boxes{i_obj, 1} = box;
            object_boxes{i_obj, 2} = estimated_objects(i_obj, f_object.ID);                
        end
    end


    %% Debug plot
    if(parameters.debug_plot_object_estimation)
        figure, 
        
        subplot(1, 2, 1), 
        max_ID_objects = max(OG_clustered(:));
        colores = rand(max_ID_objects, 3);
        color_img = ones(parameters.ROWS, parameters.COLS, 3);
        for i_r = 1 : parameters.ROWS
            for i_c = 1 : parameters.COLS
                if(OG_clustered(i_r, i_c) > 0)
                    color_img(i_r, i_c, :) = colores(OG_clustered(i_r, i_c), :);
                else
                    color_img(i_r, i_c, :) = 1 - ones(1, 3) * OG_occupied(i_r, i_c);
                end
            end
        end
        imshow(color_img), pause(0.1), 
        title('Occupancy grid Clustered')

        subplot(1, 2, 2), hold on, axis equal, xlim([parameters.min_x, parameters.min_x + parameters.COLS * parameters.cell_size]), ylim([parameters.min_y, parameters.min_y + parameters.ROWS * parameters.cell_size])
        
        for i_obj = 1 : size(estimated_objects, 1)

            % Plot cells
            plot(object_cells_data{i_obj}(:, 1), object_cells_data{i_obj}(:, 2), '.k')
            plot(points{i_obj}(:, 1), points{i_obj}(:, 2), '.k')

            color = colores(estimated_objects(i_obj, f_object.ID), :);

            % Plot center and velocity vector
            quiver_v(estimated_objects(i_obj, f_object.px), estimated_objects(i_obj, f_object.py), estimated_objects(i_obj, f_object.vx), estimated_objects(i_obj, f_object.vy), 'o', '-', color, 0.5);

            if(compute_polyshapes)
                plot(object_polyshapes{i_obj, 1}, 'FaceColor', color', 'EdgeColor', color, 'FaceAlpha', 0.25, 'EdgeAlpha', 1)
            end

            if(compute_bounding_boxes)
                aux_box = object_boxes{i_obj, 1};
                corners = compute_corners_of_box(aux_box(1), aux_box(2), aux_box(3), aux_box(4), aux_box(5));
                
                plot([corners(:, 1); corners(1, 1)], [corners(:, 2); corners(1, 2)], '-', 'Color', color)
                plot(aux_box(1), aux_box(2), 'x', 'Color', color)
            end
        end
        title('Extracted objects')
        pause(0.1)
    end

end