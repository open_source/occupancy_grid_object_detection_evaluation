function [x, y] = rotation_matrix(x_1, y_1, angRot)

    x = x_1 * cos(angRot) - y_1 * sin(angRot);
    y = x_1 * sin(angRot) + y_1 * cos(angRot); 
   
end