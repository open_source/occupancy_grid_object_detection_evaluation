function [cx, cy] = transform_cell_index_to_position(i_c, i_r, COLS, ROWS, min_x, min_y, cell_size)

    % COLS -> X
    % ROWS -> Y

    cx = i_c * cell_size + min_x - cell_size / 2; 
    cy = (ROWS - i_r) * cell_size + min_y + cell_size / 2;
end