function [i_c, i_r, okey] = transform_position_to_cell_index(x, y, COLS, ROWS, min_x, min_y, cell_size)
  
    % COLS -> X
    % ROWS -> Y

    i_c = ceil( round( (x - min_x) / cell_size, 6) );
    i_r = ROWS - ceil( round( (y - min_y) / cell_size, 6) ) + 1;
    
    okey = true(max(size(x)), 1);
    okey(i_c <= 0 | i_c > COLS | i_r <= 0 | i_r > ROWS) = false;
        
end