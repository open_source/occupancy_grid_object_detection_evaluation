function color = computeRGB_angle(ang)


    if(ang >= 0 && ang < pi/2)  % green (0, 1, 0) to blue (0, 0, 1)
        value_min = 0;
        value_max = pi / 2;        
        normalization = (ang - value_min) / (value_max - value_min);
        
        red = 0;
        green = 1 - normalization;
        blue = normalization;

    elseif(ang >= pi/2 && ang <= pi) % blue(0, 0, 1) to red (1, 0, 0) 
        value_min = pi / 2;
        value_max = pi;        
        normalization = (ang - value_min) / (value_max - value_min);
        
        red = normalization;
        green = 0;
        blue = 1 - normalization;
        
    elseif(ang >= -pi && ang < -pi/2) % red (1, 0, 0) to yellow (1, 1, 0) 
        value_min = -pi;
        value_max = -pi / 2;        
        normalization = (ang - value_min) / (value_max - value_min);

        red = 1;
        green = normalization;
        blue = 0;

    else  % yellow(1, 1, 0) to green (0, 1, 0)
        value_min = -pi / 2;
        value_max = 0;        
        normalization = (ang - value_min) / (value_max - value_min);
        
        red = 1 - normalization;
        green = 1;
        blue = 0;
    end   
    
    color = [red, green, blue];
end