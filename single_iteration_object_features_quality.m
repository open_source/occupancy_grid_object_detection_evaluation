function [data_OFQ, ICO_data, f_ICO_data, ICO_polyshapes] = single_iteration_object_features_quality(OG_occupied, OG_free, OG_observed_occupied, OG_vx, OG_vy, ...
    GT_objects, f_GT_objects, GT_shape, f_GT_shape, parameters)
    
    % Input
    %  - OG_occupied: array (ROWS x COLS) containing the estimation of occupied (can be either p(O) or m(O), but parameters should be tuned respectively)
    %  - OG_free: array (ROWS x COLS) containing the estimation of free (it is only needed when working with DST and plotting the results)
    %  - OG_observed_occupied: array (ROWS x COLS) containing the estimation of occupied computed only with the current frame's sensed data (observed occupancy grid)
    %  - OG_vx, OG_vy: array (ROWS x COLS) containing the estimation of the velocity in X and Y
    %    If no velocity information is included set dynamic_occupancy_grid = false   
    %  - GT_objects: array(n_gt x numel(fields(f_GT_objects))) containing the list of ground truth objects with certain object-level characteristics
    %  - f_GT_objects: struct denoting the column of each variable in GT_objects
    %  - GT_shape: cell(n_gt x 2) containing the polyshapes of the ground truth objects' boxes and convex hull of footprint_quality_reference. It is computed with compute_polyshapes_and_footprint_quality_reference.m 
    %  - f_GT_shape: struct denoting the column of each variable in GT_shape
    %  - parameters: struct contaning the parametrization
    %
    % Output
    %  - data_OFQ: table (n_gt x N) containing the data relative to the object feature quality scores. Also, information about the ground truth and the estimated objects used to compute this scores is include for debugging.
    %
    % Output additional
    %  - ICO_data: array (n_est x numel(fields(f_ICO_data))) containing object-level data of the estimated objects
    %  - f_ICO_data: struct denoting the colum of each variable in ICO_data
    %  - ICO_polyshapes: cell (n_est x 2) containing the polyshapes of the convex hull of the estimated objects 
     
    
    
    ROWS = parameters.ROWS; COLS = parameters.COLS; 
    min_x = parameters.min_x; min_y = parameters.min_y;
    cell_size = parameters.cell_size;
    
    cells_data = parameters.cells_data; 

    clustering_threshold_occupancy = parameters.clustering_threshold_occupancy;
    
    if(parameters.dynamic_occupancy_grid == false)
        OG_vx = zeros(ROWS, COLS);
        OG_vy = zeros(ROWS, COLS);
    end

    %% Ideal segmentation guided by bounding boxes
    
    % Cells that can be clusterized:
    % Ground truth bounding boxes rasterization
    GT_map = zeros(ROWS, COLS);
    num_gt_objects = size(GT_objects, 1);
    for i_gt = 1 : num_gt_objects                          
        [in, on] = inpolygon(cells_data(:, 1), cells_data(:, 2), GT_shape{i_gt, f_GT_shape.polyshape_box}.Vertices(:, 1), GT_shape{i_gt, f_GT_shape.polyshape_box}.Vertices(:, 2));

        aux_cells_gathered = in == 1 | on == 1;

        % This way the matching is already solved, each object ID corresponds with the ith ground truth element 
        for i = 1 : length(aux_cells_gathered)
            if(aux_cells_gathered(i) == 1)
                GT_map(cells_data(i, 4), cells_data(i, 3)) = i_gt;            
            end
        end

    end
    

    % Clusterized map
    OG_clustered = GT_map .* (OG_occupied > clustering_threshold_occupancy);


    
    %% Ideal clusterized map expansion
        
    kernel = parameters.kernel_8;
    for i_veces = 1 : parameters.OFQ_clustering_expanding_iterations
        
        % Expand
        new_OG_clustered = zeros(ROWS, COLS);
        for i_r = 1 : ROWS
            for i_c = 1 : COLS

                % If the current cell belongs to a cluster, check its vecinity 
                if(OG_clustered(i_r, i_c) ~= 0)
                    i_gt = OG_clustered(i_r, i_c);
    
                    for i_k = 1 : size(kernel, 1)
                        aux_ic = i_c + kernel(i_k, 1);
                        aux_ir = i_r + kernel(i_k, 2);    

                        if(aux_ic > 0 && aux_ic <= COLS && aux_ir > 0 && aux_ir <= ROWS)
                        
                            % If a neighbouring cell can be clusterized but it does not belong to a cluster yet -> expand
                            if(OG_occupied(aux_ir, aux_ic) > clustering_threshold_occupancy && OG_clustered(aux_ir, aux_ic) == 0)
                                new_OG_clustered(aux_ir, aux_ic) = i_gt;
                            end
                        end
                    end   
                end
            end
        end

        % Update cluster map
        if(sum(new_OG_clustered(:) > 0) > 0)
            OG_clustered(new_OG_clustered > 0) = new_OG_clustered(new_OG_clustered > 0);
        end
    end



    %% Ideally Clustered Objects (ICO)
    [ICO_data, f_ICO_data, ICO_cells_data, ICO_polyshapes, ICO_boxes, f_ICO_boxes] = compute_objects_from_clustered_map(...
        true, true, OG_clustered, OG_occupied, OG_observed_occupied, OG_vx, OG_vy, parameters);

    


    %% Filter objects 

    % Delete too small objects
    idx_too_small = ICO_data(:, f_ICO_data.n_cells) < parameters.OFQ_cluster_min_celdas;
    ICO_data(idx_too_small, :) = [];
    ICO_polyshapes(idx_too_small, :) = [];
    ICO_boxes(idx_too_small, :) = [];
    ICO_cells_data(idx_too_small, :) = [];

    % Delete non observed objects
    idx_not_observed = ICO_data(:, f_ICO_data.n_cells_obs) < parameters.OFQ_cluster_min_occupied_observed_cells;
    ICO_data(idx_not_observed, :) = [];
    ICO_polyshapes(idx_not_observed, :) = [];
    ICO_boxes(idx_not_observed, :) = [];
    ICO_cells_data(idx_not_observed, :) = [];

    %% Matching
    % Matching was already solved during the guided clustering, each object ID corresponds with the ith ground truth element 
    matching = zeros(num_gt_objects, 1);

    for i_obj = 1 : size(ICO_data, 1)
        i_gt = ICO_data(i_obj, f_ICO_data.ID);
        matching(i_gt) = i_obj;
    end
    
    
    

    
    %% Compute errors
    f_errors.detected = 1;
    f_errors.error_translation_gravity_center = 2;
    f_errors.error_translation_box = 3;
    f_errors.error_vel_mod = 4;
    f_errors.error_vel_ang = 5;
    f_errors.error_scale = 6;
    f_errors.error_box_ang = 7;
    f_errors.IoU_ideal = 8;
    
    dynamic_occupancy_grid = parameters.dynamic_occupancy_grid;

    errors = zeros(num_gt_objects, numel(fields(f_errors)));
    data_OFQ = zeros(num_gt_objects, size(errors, 2) + size(GT_objects, 2) + size(ICO_data, 2) + numel(fields(f_ICO_boxes)) + 1);
    data_OFQ(:, end) = parameters.iter;
    ICO_zeros = zeros(1, numel(fields(f_ICO_data)) + numel(fields(f_ICO_boxes)));
    for i_gt = 1 : num_gt_objects
        
        % If it has been detected
        i_obj = matching(i_gt);
        if(i_obj ~= 0)
            
            errors(i_gt, f_errors.detected) = 1;
            
            % Error translation based on the distance between centers - gravity center 
            errors(i_gt, f_errors.error_translation_gravity_center) = sqrt( (GT_objects(i_gt, f_GT_objects.px) - ICO_data(i_obj, f_ICO_data.px))^2 + (GT_objects(i_gt, f_GT_objects.py) - ICO_data(i_obj, f_ICO_data.py))^2 );

            % Error translation based on the distance between centers - bounding box 
            errors(i_gt, f_errors.error_translation_box) = sqrt( (GT_objects(i_gt, f_GT_objects.px) - ICO_boxes{i_obj, 1}(f_ICO_boxes.px))^2 + ...
                                                                 (GT_objects(i_gt, f_GT_objects.py) - ICO_boxes{i_obj, 1}(f_ICO_boxes.py))^2 );

            if(dynamic_occupancy_grid)
                % Error velocity module
                vel_mod_gt  = sqrt(GT_objects(i_gt, f_GT_objects.vx)^2 + GT_objects(i_gt, f_GT_objects.vy)^2); 
                vel_mod_obj = sqrt(ICO_data(i_obj, f_ICO_data.vx)^2 + ICO_data(i_obj, f_ICO_data.vy)^2);
                errors(i_gt, f_errors.error_vel_mod) = abs(vel_mod_gt - vel_mod_obj);
    
                % Error velocity angle
                vel_ang_gt  = atan2(GT_objects(i_gt, f_GT_objects.vy), GT_objects(i_gt, f_GT_objects.vx));
                vel_ang_obj = atan2(ICO_data(i_obj, f_ICO_data.vy), ICO_data(i_obj, f_ICO_data.vx));
                errors(i_gt, f_errors.error_vel_ang) = abs(substract_angles(vel_ang_gt, vel_ang_obj));
            else
                errors(i_gt, f_errors.error_vel_mod) = inf;
                errors(i_gt, f_errors.error_vel_ang) = inf;
            end
            
            % Error scale with aligned bounding boxes (just size error)
            aux_aligned_box_px  = GT_objects(i_gt, f_GT_objects.px); % aligned in location
            aux_aligned_box_py  = GT_objects(i_gt, f_GT_objects.py); % aligned in location
            aux_aligned_box_yaw = GT_objects(i_gt, f_GT_objects.yaw); % aligned in orientation
            aux_aligned_box_length = max(ICO_boxes{i_obj, 1}(f_ICO_boxes.length), ICO_boxes{i_obj, 1}(f_ICO_boxes.width)); 
            aux_aligned_box_width  = min(ICO_boxes{i_obj, 1}(f_ICO_boxes.length), ICO_boxes{i_obj, 1}(f_ICO_boxes.width));

            corners = compute_corners_of_box(aux_aligned_box_px, aux_aligned_box_py, aux_aligned_box_yaw, aux_aligned_box_length, aux_aligned_box_width);
            aux_polyshape_ICO_box_aligned = polyshape(corners(:, 1), corners(:, 2));
            errors(i_gt, f_errors.error_scale) = 1 - compute_IoU_polygons(GT_shape{i_gt, f_GT_shape.polyshape_box}, aux_polyshape_ICO_box_aligned);

            % Error orientation bounding box 
            errors(i_gt, f_errors.error_box_ang) = abs(substract_boxes_orientation(GT_objects(i_gt, f_GT_objects.yaw), ICO_boxes{i_obj, 1}(f_ICO_boxes.yaw)));

            % Intersection over union
            errors(i_gt, f_errors.IoU_ideal) = compute_IoU_polygons(GT_shape{i_gt, f_GT_shape.polyshape_box}, ICO_polyshapes{i_obj, 1});                  


            aux_ICO = [ICO_data(i_obj, :), ICO_boxes{i_obj}];
        else
            aux_ICO = ICO_zeros;
        end

        % Save data with which the error has been computed for debbuging 
        data_OFQ(i_gt, :) = [errors(i_gt, :), GT_objects(i_gt, :), aux_ICO, parameters.iter];
    end


    %% Create table for debugging
    data_OFQ = array2table(data_OFQ);  

    cont = 0;
    aux = fields(f_errors);
    variable_names = cell(1, size(data_OFQ, 2));
    for i = 1 : numel(fields(f_errors))
        cont = cont + 1;
        variable_names{cont} = aux{i};
    end
    aux = fields(f_GT_objects);
    for i = 1 : numel(fields(f_GT_objects))
        cont = cont + 1;
        variable_names{cont} = ['GT_', aux{i}];
    end
    aux = fields(f_ICO_data);
    for i = 1 : numel(fields(f_ICO_data))
        cont = cont + 1;
        variable_names{cont} = ['ICO_', aux{i}];
    end
    aux = fields(f_ICO_boxes);
    for i = 1 : numel(fields(f_ICO_boxes))
        cont = cont + 1;
        variable_names{cont} = ['ICO_Box_', aux{i}];
    end

    variable_names{end} = 'Iteration';
    data_OFQ.Properties.VariableNames = variable_names;


    %% Debug plot
    if(parameters.plot_object_feature_quality)        
    
%         figure, 
%         subplot(1, 2, 1), imagesc(GT_map), title('Ground truth rasterized')
%         subplot(1, 2, 2), imagesc(OG_clustered), title('OG ideally clusterized')
%         pause(0.1)
% 
        if(parameters.DempsterShaferTheory)            
            aux_pO = OG_occupied + 0.5 * (1 - OG_occupied - OG_free);
        else
            aux_pO = OG_occupied;
        end

        fig = figure; fig.Name = ['Object Feature Quality. Iteration ', num2str(parameters.iter)];
        if(parameters.dynamic_occupancy_grid)
            dynamic_vel = parameters.OFQ_score_velocity_threshold_for_valid_orientation;
            vel_map = sqrt(OG_vx.^2 + OG_vy.^2);
    
            color_img = ones(parameters.ROWS, parameters.COLS, 3);
            for i_c = 1 : parameters.COLS
                for i_r = 1 : parameters.ROWS
                    if(OG_clustered(i_r, i_c) > 0)
                        if(vel_map(i_r, i_c) > dynamic_vel)
                            color_img(i_r, i_c, :) = computeRGB_angle(atan2(OG_vy(i_r, i_c), OG_vx(i_r, i_c))); % Dynamic
                        else
                            color_img(i_r, i_c, :) = [0, 0, 0]; % Static
                            color_img(i_r, i_c, :) = 1 - ones(1, 3) * aux_pO(i_r, i_c); % Static ponderated by Oc.
                        end
                    else
                        color_img(i_r, i_c, :) = 1 - ones(1, 3) * aux_pO(i_r, i_c);                   
                    end
                end
            end
            imshow(color_img)
        else
            num_objetos = max(OG_clustered(:));
            colores = rand(num_objetos, 3);
            color_img = ones(ROWS, COLS, 3);
            for i_r = 1 : ROWS
                for i_c = 1 : COLS
                    if(OG_clustered(i_r, i_c) > 0)
                        color_img(i_r, i_c, :) = colores(OG_clustered(i_r, i_c), :);
                    else
                        color_img(i_r, i_c, :) = 1 - ones(1, 3) * aux_pO(i_r, i_c); 
                    end
                end
            end
            imshow(color_img)
        end

        pause(0.1), hold on, 

        title(['Object Feature Quality. Iteration ', num2str(parameters.iter)])
        
        color_gt = [0, 0.8, 0];
        color_est = [1, 0, 0];
        for i_gt = 1 : num_gt_objects
                                    
            % Velocity vector
            [i_c, i_r] = plot_point_over_pixel(GT_objects(i_gt, f_GT_objects.px), GT_objects(i_gt, f_GT_objects.py), COLS, ROWS, min_x, min_y, cell_size);
            quiver_v(i_c, i_r, GT_objects(i_gt, f_GT_objects.vx) / cell_size, -GT_objects(i_gt, f_GT_objects.vy) / cell_size, '.', '-', color_gt, 0.5);

            % Box
            aux_box = GT_shape{i_gt, f_GT_shape.polyshape_box};
            [i_c, i_r] = plot_point_over_pixel(aux_box.Vertices(:, 1), aux_box.Vertices(:, 2), COLS, ROWS, min_x, min_y, cell_size);
            plot([i_c; i_c(1)], [i_r; i_r(1)], '-', 'Color', color_gt)

            % Velocity vector
            i_obj = matching(i_gt, 1);
            if(i_obj > 0)
                                
                % Velocity vector aligned
                [i_c, i_r] = plot_point_over_pixel(GT_objects(i_gt, f_GT_objects.px), GT_objects(i_gt, f_GT_objects.py), COLS, ROWS, min_x, min_y, cell_size);
                quiver_v(i_c, i_r, ICO_data(i_obj, f_ICO_data.vx) / cell_size, -ICO_data(i_obj, f_ICO_data.vy) / cell_size, '.', '-', color_est, 0.5);        

                % Convex-hull
                aux_convexhull = ICO_polyshapes{i_obj, 1};
                [i_c, i_r] = plot_point_over_pixel(aux_convexhull.Vertices(:, 1), aux_convexhull.Vertices(:, 2), COLS, ROWS, min_x, min_y, cell_size);
                plot(polyshape(i_c, i_r), 'FaceColor', color_est', 'EdgeColor', color_est, 'FaceAlpha', 0.25, 'EdgeAlpha', 1)
            
                % Bounding box
                aux_box = ICO_boxes{i_obj, 1};
                corners = compute_corners_of_box(aux_box(1), aux_box(2), aux_box(3), aux_box(4), aux_box(5));       
                [i_c, i_r] = plot_point_over_pixel(corners(:, 1), corners(:, 2), COLS, ROWS, min_x, min_y, cell_size);
                plot([i_c; i_c(1)], [i_r; i_r(1)], '-', 'Color', color_est)

                % Text
                if(false)                    
                    text(mean(i_c), mean(i_r), ['TE ',  num2str(round(errors(i_gt, f_errors.error_translation_box), 2)), '   ', ...
                                                'VE ',  num2str(round(errors(i_gt, f_errors.error_vel_mod), 2)), '   ', ...
                                                'VOE ', num2str(round(errors(i_gt, f_errors.error_vel_ang) * 180 / pi, 2)), '   ', ...
                                                'SE ',  num2str(round(errors(i_gt, f_errors.error_scale), 2)), '   ', ...
                                                'BOE ', num2str(round(errors(i_gt, f_errors.error_box_ang), 2) * 180 / pi)])
                end

            
%                 % Checking
%                 [sqrt(GT_objects(i_gt, f_GT_objects.vx)^2  + GT_objects(i_gt, f_GT_objects.vy)^2), sqrt(ICO_object(i_obj, f_ICO_objects.vx)^2 + ICO_object(i_obj, f_ICO_objects.vy)^2), errors(i_gt, f_errors.error_vel_mod)]
%                 [atan2(GT_objects(i_gt, f_GT_objects.vy), GT_objects(i_gt, f_GT_objects.vx)), atan2(ICO_object(i_obj, f_ICO_objects.vy), ICO_object(i_obj, f_ICO_objects.vx)), errors(i_gt, f_errors.error_vel_ang)] * 180 / pi
%                 [GT_objects(i_gt, f_GT_objects.yaw), ICO_boxes{i_obj, 1}(f_ICO_boxes.yaw), errors(i_gt, f_errors.error_box_ang)] * 180 / pi
            end
        end
        pause(0.1)
    end
end