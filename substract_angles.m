function dif_ang = substract_angles(ang0, ang1)
    a = (ang0 - ang1 + pi);	
    b = 2 * pi;
	dif_ang = a - floor(a / b) * b - pi;
end