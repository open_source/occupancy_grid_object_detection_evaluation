function [p_line, p_point] = quiver_v(x, y, u, v, marker, line, color, scale_factor)

    if(length(x) == 1)
        p_point = plot(x, y, 'LineStyle', 'none', 'Marker', marker, 'Color', color);
        p_line = plot([x, x + u * scale_factor], [y, y + v * scale_factor], 'LineStyle', line, 'Color', color);
    else
        p_point = plot(x, y, 'LineStyle', 'none', 'Marker', marker, 'Color', color);

        lineas = nan(length(x) * 3, 2);
        for i = 1 : length(x)
            lineas((i - 1) * 3 + 1, :) = [x(i), y(i)];
            lineas((i - 1) * 3 + 2, :) = [x(i) + u(i) * scale_factor, y(i) + v(i) * scale_factor];
        end
        p_line = plot(lineas(:, 1), lineas(:, 2), 'LineStyle', line, 'Color', color);
    end
end