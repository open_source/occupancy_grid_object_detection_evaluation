function parameters = initialize_parameters()


%{ 
 + Occupancy grid
    - The occupancy grid is assumed to be located with respect the vehicle's reference system (VRS). 
      Thus, cells' position (with respect de VRS) is constant. 
      If not, modify the variables: min_x, min_y, cells_centers_x, cells_centers_y and cells_data 
    - The size of the cells is assumed to be constant and equal for every cell.
    - Columns refer to the distance towards the vehicle along the X edge, and Rows along the Y edge 
 + Ground truth data
    - Ground truth data should be refered to VRS
    - Ground truth objects should not be contained ones inside others (e.g. delete people inside vehicles)
    - Ground truth objects outside the OG range have to be deleted
    - Point cloud data is only required for the footprint quality reference and its ground truth is recomended but not essential 

 + We have chosen a conservative parameterization, e.g. a cell is occupied if m(O) > 0.1 or clusters with at least one cell are accepted. 
   But you may adapt these for your own requirements.    
%}


    %% Parameters corresponding to each occupancy grid
    parameters.cell_size = 0.15;
    parameters.ROWS = 512; % Y edge
    parameters.COLS = 512; % X edge
    parameters.min_x = -38.4; % e.g. The ego-vehicle is located at the center of an OG with cell size 0.15 and matrix size 512x512: min_x = -512 * 0.15 / 2; min_y = -512 * 0.15 / 2
    parameters.min_y = -38.4; % e.g. The ego-vehicle is located at the center of the Y-axis and at the beginning of the X-axis:  min_x = 0;  min_y = -512 * 0.15 / 2

    % If the grid includes dynamics, compute the respectively scores 
    parameters.dynamic_occupancy_grid = true; 

    % The occupancy can be represented as a probability p(O) (DempsterShaferTheory == false) or as a belief mass m(O) (DempsterShaferTheory == true)   
    % This information is only required for plotting purposes
    parameters.DempsterShaferTheory = true;



    %% Evaluation framework parametrization
    % Object Detection Capability (ODC). Object Feature Quality (OFQ)

    % Clustering threshold. Cells with a p(O) or m(O) above this threshold can be occupied 
    parameters.clustering_threshold_occupancy = 0.1;
   
    % Minimum number of cells
    parameters.ODC_cluster_min_celdas = 1; % >= 
    parameters.OFQ_cluster_min_celdas = 1; % >= 
    parameters.ODC_cluster_min_occupied_observed_cells = 1; % >=
    parameters.OFQ_cluster_min_occupied_observed_cells = 1; % >=

    % Velocity threshold to consider an object as dynamic
    parameters.ODC_score_velocity_threshold_dynamic_object = 1.0;
    parameters.OFQ_score_velocity_threshold_for_valid_orientation = 1.0;

    % Object Detection Capability - Proximity clustering 
    % An 8-connected neighborhood kernel is employed assuming that: 
    % (i) compact objects are more easily detected and better described and 
    % (ii) object merging is minimized at its most. 
    parameters.ODC_clustering_kernel = 8; 

    
    % Object Detection Capability - Matching
    % A GTO is therefore considered as detected if it overlaps with the cells of at least one DCO, i.e. if occupied space has
    % been modeled in its position
    parameters.ODC_matching_min_cells_overlaping = 1; % >= 

    % Object Detection Capability - Scores
    parameters.ODC_scores_merge_ratio_area_gt_area_obj = 0.6;
    parameters.ODC_score_cells_noisy_object = 3; % >=
           
    % Object Feature Quality - Clustering
    parameters.OFQ_clustering_expanding_iterations = 3;

    % Bounding box fitting
    parameters.bounding_box_fitting_angular_step = 0.5 * pi / 180;

    % Object Feature Quality - Scores
    parameters.OFQ_MAX_MATE = 5; % Maximum position error meters
    parameters.OFQ_MAX_MASE = 1; % Maximum scale error (IoU [0, 1])
    parameters.OFQ_MAX_MAVE = 5; % Maximum velocity error m/s
    parameters.OFQ_MAX_MAVOE = 180 * pi / 180; % Maximum velocity orientation error radians
    parameters.OFQ_MAX_MABOE = 45 * pi / 180; % Maximum bounding box orientation error radians




    %% Visual debugging plots
    parameters.plot_footprint_quality_reference = false;     

    parameters.plot_object_detection_capability = true; 
    parameters.plot_object_feature_quality = true;

    parameters.debug_plot_object_estimation = false;
    


    %% Others

    parameters.iter = 0;
    parameters.TP = 1; parameters.FP = 2; parameters.TN = 3; parameters.FN = 4;


    %% Initialization

    ROWS = parameters.ROWS;
    COLS = parameters.COLS;
    cells_data = nan(ROWS * COLS, 4);
    cx = nan(ROWS, 1); 
    cy = nan(parameters.COLS, 1); 
    cell_size = parameters.cell_size; min_x = parameters.min_x; min_y = parameters.min_y;
    for i_c = 1 : COLS  
        for i_r = 1 : ROWS                         
            
            [cx(i_c), cy(i_r)] = transform_cell_index_to_position(i_c, i_r, COLS, ROWS, min_x, min_y, cell_size);
            
            cells_data((i_c - 1) * ROWS + i_r, :) = [cx(i_c), cy(i_r), i_c, i_r];
        end
    end
    parameters.cells_centers_x = cx;
    parameters.cells_centers_y = cy;
    parameters.cells_data = cells_data;
    

    parameters.kernel_8 = [];
    for i_r = -1 : 1 
        for i_c = -1 : 1
            if(i_r ~= 0 || i_c ~= 0)
                parameters.kernel_8(end + 1, :) = [i_r, i_c];
            end
        end
    end
end