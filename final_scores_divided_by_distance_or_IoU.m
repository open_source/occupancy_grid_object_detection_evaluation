function [scores_distance_ODC, scores_IoU_ODC, scores_distance_OFQ, scores_IoU_OFQ] = final_scores_divided_by_distance_or_IoU(data_OFQ, data_ODC, ...
    footprint_quality_reference, idx_desired_objects, distance_ranges, FQR_values, parameters)


    % Input
    %  - data_OFQ: data about object feature quality. If empty it wont be computed
    %  - data_ODC: data about object detection capability. If empty it wont be computed
    %  - footprint_quality_reference: array(size(GT_objects, 1), 1) contanining the footprint quality score computed with function compute_polyshapes_and_footprint_quality_reference
    %  - idx_desired_objects: array (size(data_ODC, 1) x 1) denoting which objects are selected [0, 1]. e.g. you want to compute results just for vehicles        
    %  - distance_ranges: array (M x 2) containing M ranges of distance whithin which objects will be selected. e.g. [0, 20; 20, 40; 40, inf] m  
    %  - FQR_values: array (M x 2) containing M ranges of Footprint Quality Reference whithin which objects will be selected. e.g. [0, 0.3; 0.3, 0.6; 0.6, 1.0] m 
    %  - parameters: struct contaning the parametrization 


    % Distance from the ground truth objects to the Ref. system
    distances_GT_OFQ = sqrt(data_OFQ.GT_px.^2 + data_OFQ.GT_py.^2);
    distances_GT_ODC = sqrt(data_ODC.GT_px.^2 + data_ODC.GT_py.^2);    


    % Compute scores for each range of distances
    scores_distance_OFQ = [];
    scores_distance_ODC = [];
    for i = 1 : size(distance_ranges, 1)
        
        if(~isempty(data_OFQ))
            aux_idx = idx_desired_objects & distances_GT_OFQ >= distance_ranges(i, 1) & distances_GT_OFQ <= distance_ranges(i, 2);
            scores_distance_OFQ = [scores_distance_OFQ; final_scores_object_feature_quality(data_OFQ, aux_idx, parameters)];
        end
        
        if(~isempty(data_ODC))
            aux_idx = idx_desired_objects & distances_GT_ODC >= distance_ranges(i, 1) & distances_GT_ODC <= distance_ranges(i, 2);
            scores_distance_ODC = [scores_distance_ODC; final_scores_object_detection_capability(data_ODC, aux_idx, parameters)];
        end
    end
   
    % Compute scores for each range of IoU
    scores_IoU_OFQ = [];
    scores_IoU_ODC = [];
    for i = 1 : size(FQR_values, 1)
        aux_idx = idx_desired_objects & footprint_quality_reference >= FQR_values(i, 1) & footprint_quality_reference <= FQR_values(i, 2);

        if(~isempty(data_OFQ))
            scores_IoU_OFQ = [scores_IoU_OFQ; final_scores_object_feature_quality(data_OFQ, aux_idx, parameters)];
        end

        if(~isempty(data_ODC))
            scores_IoU_ODC = [scores_IoU_ODC; final_scores_object_detection_capability(data_ODC, aux_idx, parameters)];
        end
    end
end