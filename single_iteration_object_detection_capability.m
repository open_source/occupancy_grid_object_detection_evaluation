function [data_ODC, DCO_data, f_DCO_data, DCO_polyshapes] = single_iteration_object_detection_capability(...
    OG_occupied, OG_free, OG_observed_occupied, OG_vx, OG_vy, GT_objects, f_GT_objects, GT_shape, f_GT_shape, parameters)
    
    % Input
    %  - OG_occupied: array (ROWS x COLS) containing the estimation of occupied (can be either p(O) or m(O), but parameters should be tuned respectively)
    %  - OG_free: array (ROWS x COLS) containing the estimation of free (it is only needed when working with DST and plotting the results)
    %  - OG_observed_occupied: array (ROWS x COLS) containing the estimation of occupied computed only with the current frame's sensed data (observed occupancy grid)
    %  - OG_vx, OG_vy: array (ROWS x COLS) containing the estimation of the velocity in X and Y
    %    If no velocity information is included set dynamic_occupancy_grid = false   
    %  - GT_objects: array(n_gt x numel(fields(f_GT_objects))) containing the list of ground truth objects with certain object-level characteristics
    %  - f_GT_objects: struct denoting the column of each variable in GT_objects
    %  - GT_shape: cell(n_gt x 2) containing the polyshapes of the ground truth objects' boxes and convex hull of footprint_quality_reference. It is computed with compute_polyshapes_and_footprint_quality_reference.m 
    %  - f_GT_shape: struct denoting the column of each variable in GT_shape
    %  - parameters: struct contaning the parametrization
    %
    % Output
    %  - data_ODC: table (n_gt x N) containing the data relative to the object detectetion capability scores. Also, information about the ground truth and the estimated objects used to compute this scores is include for debugging.
    %
    % Output additional
    %  - DCO_data: array (n_est x numel(fields(f_DCO_data))) containing object-level data of the estimated objects
    %  - f_DCO_data: struct denoting the colum of each variable in DCO_data
    %  - DCO_polyshapes: cell (n_est x 2) containing the polyshapes of the convex hull of the estimated objects 
    


    ROWS = parameters.ROWS;
    COLS = parameters.COLS;
    min_x = parameters.min_x;
    min_y = parameters.min_y;
    cell_size = parameters.cell_size;
    
    if(parameters.dynamic_occupancy_grid == false)
        OG_vx = zeros(ROWS, COLS);
        OG_vy = zeros(ROWS, COLS);
    end


    %% Clustering based on proximity between occupied cells   
    % Cells with an occupied mass higher than the threshold clustering_threshold_occupancy are clusterized with the Connected Components Clustering algorithm 
    % optional -> An 8-connected neighborhood kernel is employed assuming that: (i) compact objects are more easily detected and better described and (ii) object merging is minimized at its most.
    
    CC = bwconncomp(OG_occupied > parameters.clustering_threshold_occupancy, parameters.ODC_clustering_kernel);
    OG_clustered = zeros(ROWS, COLS);
    for i_obj = 1 : length(CC.PixelIdxList)
        OG_clustered(CC.PixelIdxList{i_obj}) = i_obj;
    end

    %% Distance-based Clustered Objects (DCO) described by polyshape and velocity mean
    [DCO_data, f_DCO_data, DCO_cells_data, DCO_polyshapes, DCO_boxes] = compute_objects_from_clustered_map(...
        true, false, OG_clustered, OG_occupied, OG_observed_occupied, OG_vx, OG_vy, parameters);

    % Delete too small objects 
    % optional -> Since this evaluation addresses scenarios with pedestrians,which can be represented by few cells, clusters are accepted despite of their size.
    idx_too_small = DCO_data(:, f_DCO_data.n_cells) < parameters.ODC_cluster_min_celdas;
    DCO_data(idx_too_small, :) = [];
    DCO_polyshapes(idx_too_small, :) = [];
    DCO_boxes(idx_too_small, :) = [];
    DCO_cells_data(idx_too_small, :) = [];

    % Delete non observed objects
    % optional -> Given that in OGs it is common to find noisy occupied cells in occluded areas, only clusters which have been observed in the current frame are
    %             accepted, i.e. clusters with at least one cell with OG_observed_occupied > ODC_cluster_min_occupied_observed_cells
    idx_not_observed = DCO_data(:, f_DCO_data.n_cells_obs) < parameters.ODC_cluster_min_occupied_observed_cells;
    DCO_data(idx_not_observed, :) = [];
    DCO_polyshapes(idx_not_observed, :) = [];
    DCO_boxes(idx_not_observed, :) = [];
    DCO_cells_data(idx_not_observed, :) = [];    


    %% Matching between ground truth bounding box and estimated objects and Detection, Split, Merge, Noise scores    
    f_clustering_score.detected = 1;
    f_clustering_score.split = 2;
    f_clustering_score.merge = 3;
    f_clustering_score.noise = 4;
    f_clustering_score.IoU = 5;

    num_GT_objects = size(GT_objects, 1);
    num_ODC_objects = size(DCO_data, 1);    
    
    clustering_score = zeros(num_GT_objects, numel(fields(f_clustering_score)));
    association_matrix_IoU = zeros(num_GT_objects, num_ODC_objects);
    matching = zeros(num_GT_objects, 2);
    ODC_cont_overlaps_with_GTs = zeros(num_ODC_objects, 1);

    for i_gt = 1 : num_GT_objects
        
        for i_obj = 1 : num_ODC_objects
            
            if(~isempty(DCO_polyshapes{i_obj, 1}) && ~isempty(GT_shape{i_gt, f_GT_shape.polyshape_box}))                                      
    
                % Compute IoU between objects with polyshapes
                [IoU, area_gt, area_obj, area_intersection] = compute_IoU_polygons(GT_shape{i_gt, f_GT_shape.polyshape_box}, DCO_polyshapes{i_obj, 1});                  

                % If polygons intersect check matching
                if(area_intersection > 1e-4)

                    % Check if there is overlap at a cell level (convexhulls may cover more space than the actually modeled as occupied)
                    [in, on] = inpolygon(DCO_cells_data{i_obj, 1}(:, 1), DCO_cells_data{i_obj, 1}(:, 2), GT_shape{i_gt, f_GT_shape.polyshape_box}.Vertices(:, 1), GT_shape{i_gt, f_GT_shape.polyshape_box}.Vertices(:, 2));
                    cells_gathered =  in == 1 | on == 1;

                    % A GTO is therefore considered as detected if it overlaps with the cells of at least one DCO, i.e. if occupied space has been modeled in its position.
                    % The number of cells can be modified
                    if(sum(cells_gathered) >= parameters.ODC_matching_min_cells_overlaping)
    
                        % If more than one DCO overlaps a GTO, the one with highest IoU is associated, being the IoU computed between the bounding box of the GTO and the convex hull of the cells of the DCO.
                        association_matrix_IoU(i_gt, i_obj) = IoU;
    
                        % Detection: A GTO is considered as detected if it overlaps with the cells of at least one DCO,
                        clustering_score(i_gt, f_clustering_score.detected) = 1;                                  

                        % Split count
                        clustering_score(i_gt, f_clustering_score.split) = clustering_score(i_gt, f_clustering_score.split) + 1;                             
                                     
                        % Merge: A GT object is considered as merged if its area is far smaller than the area of its associated DCO 
                        if((area_gt / area_obj) < parameters.ODC_scores_merge_ratio_area_gt_area_obj)
                            clustering_score(i_gt, f_clustering_score.merge) = 1;
                        end

                        % Save that this ODC has overlapped a GT object
                        ODC_cont_overlaps_with_GTs(i_obj) = ODC_cont_overlaps_with_GTs(i_obj) + 1;
                    end                    
                end
            end
        end

        % Solve matching
        [v_IoU, i_obj] = max(association_matrix_IoU(i_gt, :));
        if(v_IoU > 1e-4)
            matching(i_gt, :) = [i_obj, v_IoU];
        end
    end

    % Detection score 
    clustering_score(:, f_clustering_score.detected) = clustering_score(:, f_clustering_score.detected) > 0;

    % Split score
    clustering_score(:, f_clustering_score.split) = clustering_score(:, f_clustering_score.split) > 1;

    % Score merge & noise
    for i_gt = 1 : num_GT_objects
        if(matching(i_gt, 2) > 1e-4)
            i_obj = matching(i_gt, 1);

            % A detected GT object is labeled as merged if its associated DCO overlaps with others GT objects 
            if(ODC_cont_overlaps_with_GTs(i_obj) > 1)
                clustering_score(i_gt, f_clustering_score.merge) = 1;
            end

            % A GT object is labeled as noise if the number of cells of its associated DCO is less than ODC_score_cells_noise_object
            if(DCO_data(i_obj, f_DCO_data.n_cells) <= parameters.ODC_score_cells_noisy_object)
                clustering_score(i_gt, f_clustering_score.noise) = 1;
            end
        end
    end
    

    if(false) % Just to debug the matching
        if(parameters.DempsterShaferTheory)            
            aux_pO = OG_occupied + 0.5 * (1 - OG_occupied - OG_free);
        else
            aux_pO = OG_occupied;
        end
        figure, 
        imshow(1 - aux_pO), pause(0.1)
        hold on, 

        num_objetos = max(OG_clustered(:));
        colores = rand(num_objetos, 3);
        for i_gt = 1 : size(GT_objects, 1)
            i_obj = matching(i_gt);

            if(i_obj > 0)
                color = colores(DCO_data(i_obj, f_DCO_data.ID), :);
                
                [i_c, i_r] = plot_point_over_pixel(DCO_polyshapes{i_obj}.Vertices(:, 1), DCO_polyshapes{i_obj}.Vertices(:, 2), COLS, ROWS, min_x, min_y, cell_size);
                plot(polyshape(i_c, i_r), 'FaceColor', color, 'FaceAlpha', 0.3, 'EdgeColor', color)

            else
                color = 'k';
            end
            [i_c, i_r] = plot_point_over_pixel(GT_shape{i_gt, f_GT_shape.polyshape_box}.Vertices(:, 1), GT_shape{i_gt, f_GT_shape.polyshape_box}.Vertices(:, 2), COLS, ROWS, min_x, min_y, cell_size);
            plot([i_c; i_c(1)], [i_r; i_r(1)], '-', 'Color', color, 'LineWidth', 2)
               
        end
    end

    %% Dynamic and IoU scores
    score_dynamic = zeros(num_GT_objects, 1); 
    TP = parameters.TP; FP = parameters.FP; TN = parameters.TN; FN = parameters.FN;
    dynamic_occupancy_grid = parameters.dynamic_occupancy_grid;   

    mod_vel_gt = sqrt(GT_objects(:, f_GT_objects.vx).^2 + GT_objects(:, f_GT_objects.vy).^2); % if(dynamic_occupancy_grid) mod_vel_gt(:) is 0
    mod_vel_DCO = sqrt(DCO_data(:, f_DCO_data.vx).^2 + DCO_data(:, f_DCO_data.vy).^2);

    vel_dynamic_object = parameters.ODC_score_velocity_threshold_dynamic_object;

    for i_gt = 1 : num_GT_objects
        
        % If the ground truth object has been matched to an estimated object
        if(matching(i_gt, 2) > 1e-4)

            % For every accepted cluster, the convex hull of the cells is computed, representing the estimated shape 
            clustering_score(i_gt, f_clustering_score.IoU) = matching(i_gt, 2);
            
            if(dynamic_occupancy_grid)
                
                % A label concerning its dynamic state – dynamic or static – is calculated with respect the weighted average velocity of all cells and the threshold value vel_dynamic_object 
                                
                i_obj = matching(i_gt, 1);                

                if(mod_vel_DCO(i_obj) >= vel_dynamic_object && mod_vel_gt(i_gt) >= vel_dynamic_object) % TP -> both dynamic
                    score_dynamic(i_gt) = TP;

                elseif(mod_vel_DCO(i_obj) >= vel_dynamic_object && mod_vel_gt(i_gt) < vel_dynamic_object) % FP -> estimated = dynamic, gt = static
                    score_dynamic(i_gt) = FP;

                elseif(mod_vel_DCO(i_obj) < vel_dynamic_object && mod_vel_gt(i_gt) < vel_dynamic_object) % TN -> both static
                    score_dynamic(i_gt) = TN;

                elseif(mod_vel_DCO(i_obj) < vel_dynamic_object && mod_vel_gt(i_gt) >= vel_dynamic_object) % FN -> estimated = static, gt = dynamic
                    score_dynamic(i_gt) = FN;

                else
                    wroooong
                end
            end
        end        
    end    



    %% Unify data in a single table
    aux_obj_zeros = zeros(1, size(DCO_data, 2));
    data_ODC = zeros(num_GT_objects, size(clustering_score, 2) + size(score_dynamic, 2) + size(GT_objects, 2) + size(DCO_data, 2) + 1);

    iteration = parameters.iter;

    for i_gt = 1 : num_GT_objects
        
        % If the ground truth object has been matched to an estimated object save it 
        i_obj = matching(i_gt, 1);
        if(i_obj > 0)
            aux_obj = DCO_data(i_obj, :);
        else
            aux_obj = aux_obj_zeros;
        end

        data_ODC(i_gt, :) = [clustering_score(i_gt, :), score_dynamic(i_gt, 1), GT_objects(i_gt, :), aux_obj, iteration];
    end



    %% Create table for debugging
    data_ODC = array2table(data_ODC);  

    cont = 0;
    % Score clustering
    aux = fields(f_clustering_score);
    variable_names = cell(1, size(data_ODC, 2));
    for i = 1 : numel(fields(f_clustering_score))
        cont = cont + 1;
        variable_names{cont} = aux{i};
    end

    % Score dynamic
    cont = cont + 1;
    variable_names{cont} = 'Dyn_class';

    % GT objects
    aux = fields(f_GT_objects);
    for i = 1 : numel(fields(f_GT_objects))
        cont = cont + 1;
        variable_names{cont} = ['GT_', aux{i}];
    end

    % DCO Objects
    aux = fields(f_DCO_data);
    for i = 1 : numel(fields(f_DCO_data))
        cont = cont + 1;
        variable_names{cont} = ['DCO_', aux{i}];
    end

    % Iteration
    variable_names{end} = 'Iteration';
    data_ODC.Properties.VariableNames = variable_names;
        
   

    %% Debug plot
    if(parameters.plot_object_detection_capability)       

        if(parameters.DempsterShaferTheory)            
            aux_pO = OG_occupied + 0.5 * (1 - OG_occupied - OG_free);
        else
            aux_pO = OG_occupied;
        end

        fig = figure; fig.Name = ['Object Detection Capability. Iteration ', num2str(parameters.iter)];        
        num_objetos = max(OG_clustered(:));
        colores = rand(num_objetos, 3);
        color_img = ones(ROWS, COLS, 3);
        for i_r = 1 : ROWS
            for i_c = 1 : COLS
                if(OG_clustered(i_r, i_c) > 0)
                    color_img(i_r, i_c, :) = colores(OG_clustered(i_r, i_c), :);
                else
                    color_img(i_r, i_c, :) = 1 - ones(1, 3) * aux_pO(i_r, i_c); 
                end
            end
        end
        imshow(color_img), pause(0.1)
        title(['Object Detection Capability. Iteration ', num2str(parameters.iter)])


        color_associated_cluster = [0, 0.3, 1];
        color_overlap_but_not_associated_cluster = [0, 0, 0];
        
        color_detected_noise = [1, 0, 1];
        color_detected_merge = [1, 0.5, 0];
        color_detected_split = [0.9, 1, 0];
        color_detected_perfectly = [0, 0.8, 0];
        color_not_detected = [1, 0, 0];

        pause(0.1), hold on, 


        p_detected_perfectly = plot(nan, nan, '.', 'Color', color_detected_perfectly);
        p_detected_noise = plot(nan, nan, '.', 'Color', color_detected_noise);
        p_detected_merge = plot(nan, nan, '.', 'Color', color_detected_merge);
        p_detected_split = plot(nan, nan, '.', 'Color', color_detected_split);
        p_not_detected = plot(nan, nan, '.', 'Color', color_not_detected);

        p_associated_cluster = plot(nan, nan, '.', 'Color', color_associated_cluster);
        p_overlap_but_not_associated_cluster = plot(nan, nan, '.', 'Color', color_overlap_but_not_associated_cluster);

        
        for i_gt = 1 : size(GT_shape, 1)
        
            if(~isempty(GT_shape{i_gt, 2}))

                % If the object has been detected
                if(clustering_score(i_gt, f_clustering_score.detected) == 1)
                    % Depending on the score, select a color for the ground truth object 
                    if(clustering_score(i_gt, f_clustering_score.noise) == 1)
                        color_box_gt = color_detected_noise;
                    elseif(clustering_score(i_gt, f_clustering_score.merge) == 1)
                        color_box_gt = color_detected_merge;
                    elseif(clustering_score(i_gt, f_clustering_score.split) == 1)
                        color_box_gt = color_detected_split;
                    else
                        color_box_gt = color_detected_perfectly;
                    end
                    
                    % Highlight the associated object
                    i_obj = matching(i_gt, 1);                    
                    [i_c, i_r] = plot_point_over_pixel(DCO_polyshapes{i_obj, 1}.Vertices(:, 1), DCO_polyshapes{i_obj, 1}.Vertices(:, 2), COLS, ROWS, min_x, min_y, cell_size);
                    plot(polyshape([i_c, i_r]), 'EdgeColor', color_associated_cluster, 'EdgeAlpha', 1, 'FaceColor', color_associated_cluster, 'FaceAlpha', 0, 'LineWidth', 1)                                       
               

                    % If the OG includes dynamics
                    if(parameters.dynamic_occupancy_grid)
                        % Center
                        [i_c, i_r] = plot_point_over_pixel(GT_objects(i_gt, f_GT_objects.px), GT_objects(i_gt, f_GT_objects.py), COLS, ROWS, min_x, min_y, cell_size);
                        quiver_v(i_c, i_r, GT_objects(i_gt, f_GT_objects.vx) / cell_size, -GT_objects(i_gt, f_GT_objects.vy) / cell_size, '.', '-', [0, 0.8, 0], 0.5);
                        
                        quiver_v(i_c, i_r, DCO_data(i_obj, f_DCO_data.vx) / cell_size, -DCO_data(i_obj, f_DCO_data.vy) / cell_size, '.', '-', [0, 0, 1], 0.5);        

                        if(score_dynamic(i_gt) == TP)
                            text(i_c, i_r, 'Dyn TP')
%                             text(i_c, i_r, ['Dyn TP ', num2str(round(mod_vel_gt(i_gt), 2)), ' ', num2str(round(mod_vel_DCO(i_obj), 2))])
                        elseif(score_dynamic(i_gt) == FP)
                            text(i_c, i_r, 'Dyn FP')
%                             text(i_c, i_r, ['Dyn FP ', num2str(round(mod_vel_gt(i_gt), 2)), ' ', num2str(round(mod_vel_DCO(i_obj), 2))])
                        elseif(score_dynamic(i_gt) == TN)
                            text(i_c, i_r, 'Dyn TN')
%                             text(i_c, i_r, ['Dyn TN ', num2str(round(mod_vel_gt(i_gt), 2)), ' ', num2str(round(mod_vel_DCO(i_obj), 2))])
                        elseif(score_dynamic(i_gt) == FN)
                            text(i_c, i_r, 'Dyn FN')
%                             text(i_c, i_r, ['Dyn FN ', num2str(round(mod_vel_gt(i_gt), 2)), ' ', num2str(round(mod_vel_DCO(i_obj), 2))])
                        else 
                            noup
                        end
                    end

                else
                    color_box_gt = color_not_detected;
                end
                
                % Plot GT box with the color relative to the detection
                [i_c, i_r] = plot_point_over_pixel(GT_shape{i_gt, f_GT_shape.polyshape_box}.Vertices(:, 1), GT_shape{i_gt, f_GT_shape.polyshape_box}.Vertices(:, 2), COLS, ROWS, min_x, min_y, cell_size);
                plot(polyshape([i_c, i_r]), 'EdgeColor', color_box_gt, 'EdgeAlpha', 1, 'FaceColor', color_box_gt, 'FaceAlpha', 0, 'LineWidth', 2)
                
            end                                               
        end


        for i_obj = 1 : num_ODC_objects
            % If the object has overlaped with a GT but it is not associated plot it
            if(ODC_cont_overlaps_with_GTs(i_obj) == 1 && isempty(find(matching(:, 1) == i_obj, 1)) )                
                [i_c, i_r] = plot_point_over_pixel(DCO_polyshapes{i_obj, 1}.Vertices(:, 1), DCO_polyshapes{i_obj, 1}.Vertices(:, 2), COLS, ROWS, min_x, min_y, cell_size);
                plot(polyshape([i_c, i_r]), 'EdgeColor', color_overlap_but_not_associated_cluster, 'EdgeAlpha', 1, 'FaceColor', color_overlap_but_not_associated_cluster, 'FaceAlpha', 0, 'LineWidth', 1) 
            end
        end
        pause(0.1)

        legend([p_detected_perfectly, p_detected_noise, p_detected_merge, p_detected_split, p_not_detected, p_associated_cluster, p_overlap_but_not_associated_cluster], ...
               {'Detected perfectly', 'Detected noise', 'Detected merge', 'Detected split', 'Not Detected', 'Associated cluster', 'Cluster overlapping but not associated'})


    end

    

end