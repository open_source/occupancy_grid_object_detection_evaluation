function [IoU, area_1, area_2, area_intersection, area_union] = compute_IoU_polygons(p1, p2)

    
    area_1 = polyarea(p1.Vertices(:, 1), p1.Vertices(:, 2));
    area_2 = polyarea(p2.Vertices(:, 1), p2.Vertices(:, 2));

    p_intersection = intersect(p1, p2);
    intersection = p_intersection.Vertices;                
    area_intersection = polyarea(intersection(:, 1), intersection(:, 2));               

    area_union = area_1 + area_2 - area_intersection;
        
    IoU = area_intersection / area_union;

    if(IoU > 1 || IoU < 0)
        disp(['IoU = ', num2str(IoU)])
        noooop
    end
end