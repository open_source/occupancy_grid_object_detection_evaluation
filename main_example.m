clear
close all

%% Load test data
load data_test

%% Initialize parameters
parameters = initialize_parameters();



%% Bucle    
data_OFQ = [];
data_ODC = [];
footprint_quality_reference = [];
for iter = 1 : size(data_test, 1)

    real_iteration     = data_test{iter, 1};
    scene              = data_test{iter, 2};
    OG_updated_oc      = data_test{iter, 3};
    OG_updated_free    = data_test{iter, 4};
    OG_observed_oc     = data_test{iter, 5};
    OG_observed_free   = data_test{iter, 6};
    OG_vx              = data_test{iter, 7};
    OG_vy              = data_test{iter, 8};
    GT_objects         = data_test{iter, 9};
    f_GT_objects       = data_test{iter, 10};
    pointcloud         = data_test{iter, 11};
    pointcloud_objects = data_test{iter, 12};
    f_pointcloud       = data_test{iter, 13};

    parameters.iter = iter;

    %% Ground truth polyshapes and footprint quality reference
    if(~isempty(pointcloud_objects))
        [iter_footprint_quality_reference, GT_shape, f_GT_shape] = compute_polyshapes_and_footprint_quality_reference(pointcloud_objects, f_pointcloud, GT_objects, f_GT_objects, parameters);
        footprint_quality_reference = [footprint_quality_reference; iter_footprint_quality_reference];        
    end
  

    %% Object detection capability
    iter_data_ODC = single_iteration_object_detection_capability(OG_updated_oc, OG_updated_free, OG_observed_oc, OG_vx, OG_vy, GT_objects, f_GT_objects, GT_shape, f_GT_shape, parameters);    
    data_ODC = [data_ODC; iter_data_ODC];


    %% Object features quality
    [iter_data_OFQ, ICO_data, f_ICO_data, ICO_polyshapes] = single_iteration_object_features_quality(OG_updated_oc, OG_updated_free, OG_observed_oc, OG_vx, OG_vy, GT_objects, f_GT_objects,GT_shape, f_GT_shape, parameters);
    data_OFQ = [data_OFQ; iter_data_OFQ];

end

%% General score
% Here you can specify which objects you want to include in the final score
idx_desired_objects = ones(size(data_ODC, 1), 1); 

score_ODC = final_scores_object_detection_capability(data_ODC, idx_desired_objects, parameters);
score_OFQ = final_scores_object_feature_quality(data_OFQ, idx_desired_objects, parameters);
OES = compute_overall_score(score_ODC, score_OFQ);


%% Plots example
distance_ranges = [0, 20; 20, 40; 40, 60];
FQR_values = [0, 0.33; 0.33, 0.66; 0.66, 1];
[scores_distance_ODC, scores_IoU_ODC, scores_distance_OFQ, scores_IoU_OFQ] = final_scores_divided_by_distance_or_IoU(data_OFQ, data_ODC, footprint_quality_reference, idx_desired_objects, distance_ranges, FQR_values, parameters);


fig = figure; fig.Name = ['Final results divided by distance and footprint quality reference'];
% ODC vs distance
subplot(2, 2, 1), hold on, 
plot(mean(distance_ranges, 2), scores_distance_ODC.ODCS, '-or'), 
plot(mean(distance_ranges, 2), scores_distance_ODC.JQCS, '-ob'), 
plot(mean(distance_ranges, 2), scores_distance_ODC.mIoU_proximity, '-o', 'Color', [0, 0.8, 0]), 
plot(mean(distance_ranges, 2), scores_distance_ODC.Dyn_F1, '-o', 'Color', [1, 0.6, 0]), 
legend('ODCS', 'JQCS', 'mIoU proximity', 'Dyn F1')
title('Object Detection Capability vs Distance'), xlabel('Distance m'), ylim([-0.1, 1.1]), xlim([min(distance_ranges(:, 1)), max(distance_ranges(:, 2))])

% ODC vs footprint quality reference
subplot(2, 2, 2), hold on, 
plot(mean(FQR_values, 2), scores_IoU_ODC.ODCS, '-or'), 
plot(mean(FQR_values, 2), scores_IoU_ODC.JQCS, '-ob'), 
plot(mean(FQR_values, 2), scores_IoU_ODC.mIoU_proximity, '-o', 'Color', [0, 0.8, 0]), 
plot(mean(FQR_values, 2), scores_IoU_ODC.Dyn_F1, '-o', 'Color', [1, 0.6, 0]), 
legend('ODCS', 'JQCS', 'mIoU proximity', 'Dyn F1')
title('Object Detection Capability vs Footprint Quality Reference'), xlabel('Footprint Quality Reference m'), ylim([-0.1, 1.1]), xlim([min(FQR_values(:, 1)), max(FQR_values(:, 2))])

% ODC vs distance
subplot(2, 2, 3), hold on, 
plot(mean(distance_ranges, 2), scores_distance_OFQ.JFMS, '-or'), 
plot(mean(distance_ranges, 2), scores_distance_OFQ.mIoU_ideal, '-o', 'Color', [0, 0.8, 0]), 
legend('JFMS', 'mIoU ideal')
title('Object Feature Quality vs Distance'), xlabel('Distance m'), ylim([-0.1, 1.1]), xlim([min(distance_ranges(:, 1)), max(distance_ranges(:, 2))])

% ODC vs footprint quality reference
subplot(2, 2, 4), hold on, 
plot(mean(FQR_values, 2), scores_IoU_OFQ.JFMS, '-or'), 
plot(mean(FQR_values, 2), scores_IoU_OFQ.mIoU_ideal, '-o', 'Color', [0, 0.8, 0]), 
legend('JFMS', 'mIoU ideal')
title('Object Feature Quality vs Footprint Quality Reference'), xlabel('Footprint Quality Reference m'), ylim([-0.1, 1.1]), xlim([min(FQR_values(:, 1)), max(FQR_values(:, 2))])
