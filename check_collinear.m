function areCollinear = check_collinear(xy) 
    
    areCollinear = rank(xy(2:end,:) - xy(1,:)) == 1;
    
end