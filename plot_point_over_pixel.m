function [plot_x, plot_y, okey] = plot_point_over_pixel(x, y, COLS, ROWS, min_x, min_y, cell_size)

    [i_x, i_y, okey] = transform_position_to_cell_index(x, y, COLS, ROWS, min_x, min_y, cell_size);
    if(okey == false)
        plot_x = i_x;
        plot_y = i_y;
        return
    end

    [cx, cy] = transform_cell_index_to_position(i_x, i_y, COLS, ROWS, min_x, min_y, cell_size);

    % (i_x + 0.5) = m * (cx + cell_size / 2) + n
    % (i_x - 0.5) = m * (cx - cell_size / 2) + n
    % -> 1 = m * cell_size -> m = 1 / cell_size;
    % --> n = i_x - 0.5 - m * (cx - cell_size / 2);
    m = 1 / cell_size;
    n = i_x - 0.5 - m * (cx - cell_size / 2);
    plot_x = m * x + n;


    % (i_y + 0.5) = m * (cy - cell_size / 2) + n
    % (i_y - 0.5) = m * (cy + cell_size / 2) + n
    % -> 1 = - m * cell_size -> m = - 1 / cell_size;
    % --> n = i_y - 0.5 - m * (cy + cell_size / 2);
    m = - 1 / cell_size;
    n = i_y - 0.5 - m * (cy + cell_size / 2);
    plot_y = m * y + n;

end