function [result, ang1_reoriented, ang2_reoriented] = substract_boxes_orientation(ang_fix, ang_movable)

    % Either side of a bounding box can denote the correct orientation
    % Thus the smallest difference is selected

    delta(1) = substract_angles(ang_fix, ang_movable);
    delta(2) = substract_angles(ang_fix, fix_angle_rad(ang_movable + pi / 2));
    delta(3) = substract_angles(ang_fix, fix_angle_rad(ang_movable + pi));
    delta(4) = substract_angles(ang_fix, fix_angle_rad(ang_movable + 3 / 2 * pi));    

    [~, i] = min(abs(delta));

    result = delta(i);

    if(i == 1)
        ang1_reoriented = ang_fix;
        ang2_reoriented = ang_movable;
    end
    if(i == 2)
        ang1_reoriented = ang_fix;
        ang2_reoriented = fix_angle_rad(ang_movable + pi / 2);
    end
    if(i == 3)
        ang1_reoriented = ang_fix;
        ang2_reoriented = fix_angle_rad(ang_movable + pi);
    end
    if(i == 4)
        ang1_reoriented = ang_fix;
        ang2_reoriented = fix_angle_rad(ang_movable + 3 / 2 * pi);
    end

end